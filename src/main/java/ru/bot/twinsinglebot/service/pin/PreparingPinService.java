package ru.bot.twinsinglebot.service.pin;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.component.event.UpdateEvent;
import ru.bot.twinsinglebot.config.TelegramBotProperties;
import ru.bot.twinsinglebot.data.pojo.pin.preparing.PreparingPin;
import ru.bot.twinsinglebot.data.pojo.pin.preparing.PreparingUserStatus;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;
import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;
import static org.apache.commons.lang3.StringUtils.joinWith;
import static org.apache.commons.lang3.StringUtils.startsWithAny;

@Slf4j
@Service
public class PreparingPinService extends AbstractPinService<PreparingPin, PreparingUserStatus> {

    private final AtomicInteger dateBound = new AtomicInteger();

    private final Map<String, String> buttons = new TreeMap<>(Map.of(
            "0", "Буду на битве",
            "1", "Буду афк в дефе",
            "2", "Буду с банками"
    ));

    public PreparingPinService(
            TelegramBot bot,
            TelegramBotProperties props,
            UserRepository userRepository
    ) {
        super(bot, props, userRepository);
    }

    @Override
    protected boolean checkText(String text) {
        return startsWithAny(text, "пин перекличка ");
    }

    @Override
    protected String getText(String text) {
        return text.substring(15);
    }

    @Override
    protected void customizeMessage(PreparingPin pin, Message message, SendMessage sendMessage) {
        pin.setKeyboard(getReplyKeyboard(message));
        sendMessage.setReplyMarkup(pin.getKeyboard());
    }

    @Override
    protected void handleForward(Message message) {
    }

    @Override
    @SneakyThrows(TelegramApiException.class)
    protected void sendUsers(PreparingPin pin) {
        log.debug("sendUsers pin: {}", pin.toString().replace('\n', ' '));
        var text = preparePinMessage(pin);
        if (text.equals(pin.getMessage())) {
            log.debug("sendUsers no changes");
            return;
        }
        pin.setMessage(text);

        var editMessageText = new EditMessageText();
        editMessageText.setChatId(pin.getChatId());
        editMessageText.setMessageId(pin.getMessageId());
        editMessageText.setText(text);
        editMessageText.enableMarkdown(true);
        editMessageText.setReplyMarkup(pin.getKeyboard());

        try {
            bot.execute(editMessageText);
        } catch (TelegramApiRequestException e) {
            log.error(e.getMessage());
            log.error(e.getApiResponse());
        }
    }

    @Override
    protected String preparePinMessage(PreparingPin pin) {
        log.debug("preparePinMessage start");
        var builder = new StringBuilder();
        builder.append(pin.getHeader()).append('\n');
        var mapByDest = StreamEx.of(pin.getStatuses().values()).groupingBy(PreparingUserStatus::getSelection);
        for (var button : buttons.values()) {
            var users = mapByDest.getOrDefault(button, Collections.emptyList());
            log.debug("preparePinMessage button: {}; users: {}", button, users);
            if (users.isEmpty()) {
                continue;
            }
            builder.append('\n')
                   .append(button)
                   .append(": ")
                   .append(users.size() > 0 ? users.size() : "");
            StreamEx.of(users).map(this::getUserMention).forEach(builder::append);
            builder.append('\n');
        }
        return builder.toString();
    }

    private String getUserMention(PreparingUserStatus userStatus) {
        var user = userStatus.getUser();
        if (user != null) {
            return format("\n[%s](tg://user?id=%d)\\[%d]",
                    user.getNick(),
                    user.getId(),
                    user.getLevel());
        } else {
            var name = joinWith(" ", userStatus.getFirstName(), userStatus.getLastName()).trim();
            return format("\n[%s](tg://user?id=%d)", name, userStatus.getTgUserId());
        }
    }

    @Override
    protected void checkStatus() {
    }

    private InlineKeyboardMarkup getReplyKeyboard(Message message) {
        var keyboardMarkup = new InlineKeyboardMarkup();
        var keyboard = List.of(
                List.of(new InlineKeyboardButton(buttons.get("0"))),
                List.of(new InlineKeyboardButton(buttons.get("1"))),
                List.of(new InlineKeyboardButton(buttons.get("2")))
        );
        keyboard.get(0).get(0).setCallbackData(prepareKeyboardButtonData(message, "0"));
        keyboard.get(1).get(0).setCallbackData(prepareKeyboardButtonData(message, "1"));
        keyboard.get(2).get(0).setCallbackData(prepareKeyboardButtonData(message, "2"));
        keyboardMarkup.setKeyboard(keyboard);

        return keyboardMarkup;
    }

    private String prepareKeyboardButtonData(Message message, String val) {
        return format("PreparingPin:%d:%d:%s", message.getChatId(), message.getDate(), val);
    }

    @Async
    @EventListener(UpdateEvent.class)
    public void updateHandler(UpdateEvent event) {
        var update = event.getUpdate();
        if (!update.hasCallbackQuery()) {
            return;
        }
        log.debug("updateHandler update: {}", update.toString().replace('\n', ' '));
        var callbackQuery = update.getCallbackQuery();
        var queryData = callbackQuery.getData().split(":");
        if (!"PreparingPin".equals(queryData[0])) {
            return;
        }
        var chatId = parseLong(queryData[1]);
        var date = parseInt(queryData[2]);
        var msg = queryData[3];
        if (date < dateBound.get()) {
            return;
        }
        var pin = pins.get(chatId);
        if (pin == null) {
            return;
        }
        var from = callbackQuery.getFrom();
        var userStatus = pin.getStatuses().computeIfAbsent(from.getId(), k -> new PreparingUserStatus());
        if (userStatus.getDebounce() >= currentTimeMillis()) {
            return;
        }
        userStatus.setTgUserId(from.getId());
        userRepository.findById(from.getId()).ifPresent(userStatus::setUser);
        userStatus.setSelection(buttons.get(msg));
        userStatus.setDebounce(currentTimeMillis() + 3000);
        sendUsers(pin);
    }

    @Scheduled(cron = "0 0 9,10,14,15,19,20 * * *", zone = "GMT+3")
    void clearPins() {
        log.debug("clearPins start");
        pins.clear();
        dateBound.set((int) (currentTimeMillis() / 1000));
        log.debug("clearPins end");
    }

}
