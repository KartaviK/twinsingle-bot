package ru.bot.twinsinglebot.service.pin;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.RfInfoMessageParser;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.config.TelegramBotProperties;
import ru.bot.twinsinglebot.data.pojo.pin.Destination;
import ru.bot.twinsinglebot.data.pojo.pin.cw.ChipWarPin;
import ru.bot.twinsinglebot.data.pojo.pin.cw.ChipWarUserStatus;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.String.format;
import static java.time.format.DateTimeFormatter.ISO_TIME;
import static org.apache.commons.lang3.StringUtils.joinWith;
import static org.apache.commons.lang3.StringUtils.startsWithAny;
import static ru.bot.twinsinglebot.data.pojo.pin.Destination.AQUILLA;
import static ru.bot.twinsinglebot.data.pojo.pin.Destination.BASILARIS;
import static ru.bot.twinsinglebot.data.pojo.pin.Destination.CASTITAS;
import static ru.bot.twinsinglebot.data.pojo.pin.Destination.GENERAL_STAFF;
import static ru.bot.twinsinglebot.data.pojo.pin.Destination.MINE;
import static ru.bot.twinsinglebot.data.pojo.pin.UserStatus.DEAD;
import static ru.bot.twinsinglebot.data.pojo.pin.UserStatus.ON_THE_WAY;
import static ru.bot.twinsinglebot.data.pojo.pin.UserStatus.READY;

@Slf4j
@Service
public class ChipWarPinService extends AbstractPinService<ChipWarPin, ChipWarUserStatus> {

    private final Map<Integer, Destination> destinations = new ConcurrentHashMap<>();

    private final ObjectMapper objectMapper;

    private final RfInfoMessageParser rfInfoMessageParser;

    private final StringRedisConnection stringRedisConnection;

    public ChipWarPinService(
            ObjectMapper objectMapper,
            RfInfoMessageParser rfInfoMessageParser,
            StringRedisConnection stringRedisConnection,
            TelegramBot bot,
            TelegramBotProperties props,
            UserRepository userRepository
    ) {
        super(bot, props, userRepository);
        this.objectMapper = objectMapper;
        this.rfInfoMessageParser = rfInfoMessageParser;
        this.stringRedisConnection = stringRedisConnection;
    }

    @PostConstruct
    private void init() {
        stringRedisConnection.subscribe(this::onMessage, "rf_info_msg");
    }

    @Override
    protected boolean checkText(String text) {
        return text.startsWith("пин ") && !startsWithAny(text, "пин замки ", "пин замок ", "пин перекличка ");
    }

    @Override
    protected String getText(String text) {
        return text.substring(4);
    }

    @Override
    protected void handleForward(Message message) {
        var msg = message.getText();
        var chatId = message.getChatId();
        var pin = pins.get(chatId);
        if (pin == null) {
            return;
        }
        var from = message.getFrom();
        var fromId = from.getId();
        var fwdId = message.getForwardFrom().getId();
        log.debug("handleForward chatId: {}; fromId: {}; fwdId: {}; msg: {}", chatId, fromId, fwdId, msg);
        if (fwdId.equals(fromId)) {
            switch (msg) {
                case "\uD83D\uDC69\u200D\uD83D\uDE80 Терминал Basilaris":
                    destinations.put(fromId, BASILARIS);
                    break;
                case "\uD83E\uDDDD\u200D♀ Терминал Castitas":
                    destinations.put(fromId, CASTITAS);
                    break;
                case "\uD83E\uDD16 Терминал Aquilla":
                    destinations.put(fromId, AQUILLA);
                    break;
                case "\uD83C\uDF0B Краговые шахты":
                case "\uD83C\uDFDB В ген. штаб":
                    break;
                default:
                    return;
            }
            tryRemoveMessage(message);
            return;
        }

        var userStatus = obtainUserStatus(pin, from);

        if (msg.startsWith("Ты направляешься в краговые шахты")) {
            userStatus.setDestination(MINE);
            userStatus.setStatus(ON_THE_WAY);
            userStatus.setArriveTime(obtainArrivalTime(message));
        } else if (msg.startsWith("Ты направляешься к терминалу")) {
            var destination = destinations.get(fromId);
            if (destination == null) {
                sendMeTerminal(message, "Сначала скажи мне, куда ты идёшь!");
                return;
            }
            userStatus.setDestination(destination);
            userStatus.setStatus(ON_THE_WAY);
            userStatus.setArriveTime(obtainArrivalTime(message));
        } else if (msg.startsWith("Ты направляешься в ген. штаб")) {
            userStatus.setDestination(GENERAL_STAFF);
            userStatus.setStatus(ON_THE_WAY);
            userStatus.setArriveTime(obtainArrivalTime(message));
        } else if (msg.startsWith("Ты прибыл в краговые шахты")) {
            userStatus.setDestination(MINE);
            userStatus.setStatus(READY);
            userStatus.setArriveTime(obtainArrivalTime(message, 0));
        } else if (msg.startsWith("Ты прибыл к терминалу")) {
            var destination = Destination.valueOf(msg.substring(24, msg.indexOf(',')).toUpperCase());
            userStatus.setDestination(destination);
            userStatus.setStatus(READY);
            userStatus.setArriveTime(obtainArrivalTime(message, 0));
        } else if (msg.equals("Ты воскрес!")) {
            if (userStatus.getDestination() == null) {
                sendMeTerminal(message, "Сначала скажи мне, на каком ты терминале!");
            }
            userStatus.setStatus(READY);
            userStatus.setArriveTime(obtainArrivalTime(message, 0));
        } else {
            return;
        }
        pin.getStatuses().putIfAbsent(fromId, userStatus);
        tryRemoveMessage(message);
    }

    @Override
    protected String preparePinMessage(ChipWarPin pin) {
        log.debug("preparePinMessage start");
        var builder = new StringBuilder();
        builder.append(pin.getHeader()).append('\n');
        var mapByDest = StreamEx.of(pin.getStatuses().values()).groupingBy(ChipWarUserStatus::getDestination);
        for (var dest : Destination.values()) {
            var users = mapByDest.getOrDefault(dest, Collections.emptyList());
            if (users.isEmpty()) {
                continue;
            }
            log.debug("preparePinMessage dest: {}; users: {}", dest, users);
            users.sort(Comparator.comparing(ChipWarUserStatus::getArriveTime));
            builder.append('\n')
                   .append(dest.value())
                   .append(": ")
                   .append(users.size() > 0 ? users.size() : "");
            StreamEx.of(users).map(this::getUserMention).forEach(builder::append);
            builder.append('\n');
        }
        return builder.toString();
    }

    private String getUserMention(ChipWarUserStatus userStatus) {
        var user = userStatus.getUser();
        var time = userStatus.getArriveTime().toLocalTime().plusHours(3).format(ISO_TIME);
        if (user != null) {
            var status = userStatus.getStatus() != null ? " " + userStatus.getStatus().icon() : "";
            return format("\n[%s](tg://user?id=%d)\\[%d]%s %s",
                    user.getNick(),
                    user.getId(),
                    user.getLevel(),
                    status,
                    time);
        } else {
            var name = joinWith(" ", userStatus.getFirstName(), userStatus.getLastName()).trim();
            var status = userStatus.getStatus() != null ? " " + userStatus.getStatus().icon() : "";
            return format("\n[%s](tg://user?id=%d)%s %s", name, userStatus.getTgUserId(), status, time);
        }
    }

    @SneakyThrows(TelegramApiException.class)
    private void sendMeTerminal(Message message, String text) {
        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChatId());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);
        bot.execute(sendMessage);
    }

    @Scheduled(cron = "0 0 9,11,14,16,19,21 * * *", zone = "GMT+3")
    void clearPins() {
        log.debug("clearPins start");
        pins.clear();
        destinations.clear();
        log.debug("clearPins end");
    }

    @SneakyThrows
    private void onMessage(org.springframework.data.redis.connection.Message message, @Nullable byte[] pattern) {
        var msg = objectMapper.readValue(message.getBody(), ru.bot.twinsinglebot.data.document.Message.class);
        log.debug("onMessage msg: {}", msg.toString().replace('\n', ' '));
        var text = msg.getText();
        if (!text.startsWith("<==Терминал")) {
            return;
        }
        var destination = Destination.valueOf(text.substring(12, text.indexOf('=', 12)).toUpperCase());
        var results = rfInfoMessageParser.computeResults(List.of(msg));
        for (var pin : pins.values()) {
            var statuses = pin.getStatuses();
            for (var result : results) {
                if (result.getLoses() == 0) {
                    continue;
                }
                var nick = result.getNick().substring(result.getNick().indexOf(']') + 1);
                var byNick = userRepository.findByNick(nick);
                if (byNick.isEmpty()) {
                    continue;
                }
                var user = byNick.get();
                var userId = user.getId();
                if (!statuses.containsKey(userId)) {
                    continue;
                }
                var userStatus = statuses.get(userId);
                userStatus.setDestination(destination);
                userStatus.setStatus(DEAD);
                userStatus.setArriveTime(obtainArrivalTime(msg.getDate(), 600));
            }
        }
    }

}
