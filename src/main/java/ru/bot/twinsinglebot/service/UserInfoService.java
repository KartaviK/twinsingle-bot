package ru.bot.twinsinglebot.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.event.NewMessageEvent;
import ru.bot.twinsinglebot.data.pojo.User;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.util.Arrays.binarySearch;
import static ru.bot.twinsinglebot.data.enums.Totem.ARES;
import static ru.bot.twinsinglebot.data.enums.Totem.CRONUS;
import static ru.bot.twinsinglebot.data.enums.Totem.HEPHAESTUS;
import static ru.bot.twinsinglebot.data.enums.Totem.POSEIDON;
import static ru.bot.twinsinglebot.data.enums.Totem.ZEUS;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserInfoService {

    private final Pattern userInfoPattern = Pattern.compile("" +
            "Раса: \\W+(?<race>\\w+)\\n" +
            "Ник: (?<guild>\\[[^]]+])?(?<nick>[^\\n]+)\\n" +
            "Идентификатор: (?<id>[\\da-h]+)\\n.+?" +
            "Здоровье: (?:-?[\\d.]+)/(?<hp>[\\d.]+)\\n.+?" +
            "Уровень: (?<lvl>\\d+)\\n(?:\\n|.)+?" +
            "Атака: 45 \\((?<dmg>[+-]?[\\d.]+)\\).+?" +
            "Защита:.+\\((?<def>\\+[\\d.]+)\\)\\n.+?" +
            "Уворот:.+?\\((?<dodge>\\+[\\d.]+)%\\).+?" +
            "Крит:.+?\\((?<crit>\\+[\\d.]+)%\\)\\n.+?" +
            "Доп\\. опыт: (?<exp>[\\d.]+)%.+?" +
            "Доп\\. аден: (?<gold>[\\d.]+)%");

    private final Pattern numberPattern = Pattern.compile("[\\d.]+");

    private final UserRepository userRepository;

    @Async
    @EventListener(NewMessageEvent.class)
    @SneakyThrows(TelegramApiException.class)
    public void onEvent(NewMessageEvent event) {
        var message = event.getMessage();
        if (message.getForwardFrom() == null || message.getForwardFrom().getId() != 577009581) {
            return;
        }
        if (!message.hasText()) {
            return;
        }
        var text = message.getText();
        if (!text.startsWith("Раса:")) {
            return;
        }
        log.debug("onEvent text: {}", text.replace('\n', ' '));
        var user = parseUser(userInfoPattern.matcher(text));
        if (user == null) {
            return;
        }
        parseTotems(user, text);
        var from = message.getFrom();
        user.setId(from.getId());
        user.setFirstName(from.getFirstName());
        user.setLastName(from.getLastName());
        user.setUserName(from.getUserName());
        user.setProfileDate(message.getForwardDate());
        user.setLastUpdate(message.getDate());
        log.debug("onEvent user: {}", user.toString().replace('\n', ' '));

        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChat().getId());
        sendMessage.setReplyToMessageId(message.getMessageId());

        try {
            userRepository.save(user);
            sendMessage.setText("Схоронил");
        } catch (DuplicateKeyException e) {
            sendMessage.setText("Профиль с таким ником уже зарегистрирован!");
        }

        event.getBot().execute(sendMessage);
    }

    private User parseUser(Matcher matcher) {
        if (matcher.find()) {
            var user = new User();
            user.setRace(matcher.group("race"));
            user.setGuild(matcher.group("guild"));
            user.setNick(matcher.group("nick"));
            user.setRfId(matcher.group("id"));
            user.setHealth(parseDouble(matcher.group("hp")));
            user.setLevel(parseInt(matcher.group("lvl")));
            user.setDamage(45 + parseDouble(matcher.group("dmg")));
            user.setDefense(20 + parseDouble(matcher.group("def")));
            user.setDodge(3 + parseDouble(matcher.group("dodge")));
            user.setPrecision(10 + parseDouble(matcher.group("crit")));
            user.setExpBonus(new BigDecimal(matcher.group("exp")));
            user.setGold(new BigDecimal(matcher.group("gold")));
            log.debug("parseUser user: {}", user);
            return user;
        }
        return null;
    }

    private void parseTotems(User user, String msg) {
        for (var line : msg.split("\\n+")) {
            if (line.contains("Бафф")) {
                var matcher = numberPattern.matcher(line);
                if (matcher.find()) {
                    var val = Double.parseDouble(matcher.group(0));
                    if (line.contains("Ареса")) {
                        user.setAres(binarySearch(ARES.stats(), val));
                    } else if (line.contains("Посейдона")) {
                        user.setPoseidon(binarySearch(POSEIDON.stats(), val));
                    } else if (line.contains("Гефеста")) {
                        user.setHephaestus(binarySearch(HEPHAESTUS.stats(), val));
                    } else if (line.contains("️Зевса")) {
                        user.setZeus(binarySearch(ZEUS.stats(), val));
                    } else if (line.contains("️Кроноса")) {
                        user.setCronus(binarySearch(CRONUS.stats(), val));
                    }
                }
            }
        }
        log.debug("parseTotems user: {}", user);
    }

}
