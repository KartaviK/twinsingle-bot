package ru.bot.twinsinglebot.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.event.NewMessageEvent;
import ru.bot.twinsinglebot.data.pojo.User;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

import java.util.Optional;
import java.util.regex.Pattern;

import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class DungeonWaitPingService {

    private final Pattern pattern = Pattern.compile("^(\\[[^]]+])?(.+)$");

    private final UserRepository userRepository;

    @Async
    @EventListener(NewMessageEvent.class)
    @SneakyThrows(TelegramApiException.class)
    public void handleMessage(NewMessageEvent event) {
        var message = event.getMessage();
        if (message.getForwardFrom() == null || message.getForwardFrom().getId() != 577009581) {
            return;
        }
        if (!message.hasText()) {
            return;
        }
        var text = message.getText();
        if (!text.contains("не в ген. штабе")) {
            return;
        }
        log.debug("handleMessage text: {}", text.replace('\n', ' '));

        text = text.substring(1)
                   .replace(" не в ген. штабе]", "")
                   .replace(", ", ",")
                   .replace("\uD83D\uDC69\u200D\uD83D\uDE80", "");

        var builder = new StringBuilder("Кабанчиком в гш");

        for (var s : text.split(",")) {
            var matcher = pattern.matcher(s);
            while (matcher.find()) {
                userRepository.findByNick(matcher.group(2))
                              .map(this::getUserMention)
                              .or(() -> Optional.of("\n@" + matcher.group(2)))
                              .ifPresent(builder::append);
            }
        }

        if (builder.length() == 15) {
            return;
        }

        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChat().getId());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(builder.toString());
        sendMessage.enableMarkdown(true);

        event.getBot().execute(sendMessage);
    }

    private String getUserMention(User user) {
        return format("\n[%s](tg://user?id=%d)", user.getNick(), user.getId());
    }

}
