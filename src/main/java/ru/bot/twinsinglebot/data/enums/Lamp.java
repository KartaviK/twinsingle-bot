package ru.bot.twinsinglebot.data.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@Accessors(fluent = true)
@AllArgsConstructor
public enum Lamp {

    PATRIARCH("\uD83D\uDC51Патриарх", 0, 25, 25),
    ARCHON("\uD83D\uDD31Архонт", 0, 0, 40),
    ATTACKER("\uD83D\uDDE1Атакующий", 0, 20, 0),
    DEFENDER("\uD83D\uDEE1Защитник", 0, 0, 20),
    SUPPORT("\uD83D\uDCEFПоддержка", 20, 0, 15);

    private String lampName;

    private double health;

    private double damage;

    private double defense;

}
