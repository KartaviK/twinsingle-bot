package ru.bot.twinsinglebot.data.document;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Document("log")
public class LoggingEventMongo {

    @Id
    private String id;

    private String level;

    private long timeStamp;

    private String date;

    private String message;

    private String threadName;

    private String loggerName;

    private ThrowableProxyMongo proxy;

}
