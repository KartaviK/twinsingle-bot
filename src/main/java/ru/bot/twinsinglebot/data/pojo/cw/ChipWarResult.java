package ru.bot.twinsinglebot.data.pojo.cw;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document
public class ChipWarResult {

    @Id
    private int date;

    private List<ChipWarUserResult> results;

}
