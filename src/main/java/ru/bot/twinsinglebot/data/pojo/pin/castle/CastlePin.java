package ru.bot.twinsinglebot.data.pojo.pin.castle;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.bot.twinsinglebot.data.pojo.pin.AbstractPin;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CastlePin extends AbstractPin<CastleUserStatus> {

    private final Set<String> messages = Collections.newSetFromMap(new ConcurrentHashMap<>());

    private final Map<String, CastleEnemyStatus> enemies = new ConcurrentHashMap<>();

    private int startDate;

    {
        var time = (int) (System.currentTimeMillis() / 1000);
        startDate = (time - time % 86400) + 14 * 60 * 60;
    }

}
