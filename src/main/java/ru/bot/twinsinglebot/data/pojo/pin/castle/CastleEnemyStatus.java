package ru.bot.twinsinglebot.data.pojo.pin.castle;

import lombok.Data;
import ru.bot.twinsinglebot.data.pojo.pin.Destination;

@Data
public class CastleEnemyStatus {

    private final String nick;

    private Destination destination;

    private int kills;

    private int deaths;

    private int deathDate;

}
