package ru.bot.twinsinglebot.data.pojo.pin;

import lombok.Data;
import ru.bot.twinsinglebot.data.pojo.User;

import java.time.ZonedDateTime;

@Data
public abstract class AbstractUserStatus {

    private int tgUserId;

    private String firstName;

    private String lastName;

    private User user;

    private Destination destination;

    private UserStatus status;

    private ZonedDateTime arriveTime;

}
