package ru.bot.twinsinglebot.data.pojo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.bot.twinsinglebot.data.pojo.battle.Unit;

import java.math.BigDecimal;

@Data
@Document
public class User implements Unit {

    @Id
    private Integer id;

    private String firstName;

    private String lastName;

    private String userName;

    private boolean blocked;

    private int profileDate;

    private int lastUpdate;

    private String race;

    private String guild;

    @Indexed(unique = true)
    private String nick;

    @Indexed(unique = true)
    private String rfId;

    private int level;

    private double health;

    private double damage;

    private double defense;

    private double dodge;

    private double precision;

    private BigDecimal expBonus;

    private BigDecimal gold;

    private int ares;

    private int poseidon;

    private int hephaestus;

    private int zeus;

    private int cronus;

    @Override
    public int getExp() {
        return 0;
    }

}
