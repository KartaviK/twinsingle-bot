package ru.bot.twinsinglebot.data.pojo;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import ru.bot.twinsinglebot.data.enums.Lamp;

@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class LampResult {

    private Lamp lamp;

    private int wins;

}
