package ru.bot.twinsinglebot.data.pojo.pin.cw;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.bot.twinsinglebot.data.pojo.pin.AbstractPin;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ChipWarPin extends AbstractPin<ChipWarUserStatus> {

}
