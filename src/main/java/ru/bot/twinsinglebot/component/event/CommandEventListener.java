package ru.bot.twinsinglebot.component.event;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.component.handler.CommandHandler;
import ru.bot.twinsinglebot.config.TelegramBotProperties;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.apache.commons.lang3.ObjectUtils.firstNonNull;

@Slf4j
@Component
@RequiredArgsConstructor
public class CommandEventListener {

    private final AtomicBoolean silence = new AtomicBoolean(true);

    private final Set<String> silentCommands = Set.of("/myid", "/groupid", "/cwstats", "/export");

    private final TelegramBotProperties props;

    private final List<CommandHandler> commandHandlers;

    private Map<String, CommandHandler> handlerMap;

    @PostConstruct
    private void init() {
        silence.set(props.isSilence());
        handlerMap = StreamEx.of(commandHandlers).toMap(CommandHandler::getCommand, e -> e);
    }

    @Async
    @EventListener(UpdateEvent.class)
    public void onApplicationEvent(UpdateEvent event) {
        handleMessage(event.getUpdate(), event.getBot());
    }

    private void handleMessage(Update update, TelegramBot bot) {
        try {
            var command = getCommand(update);
            if (command == null) {
                return;
            }
            var message = update.getMessage();
            if ("/silence".equals(command) && "neutrino1911".equals(message.getFrom().getUserName())) {
                var mode = message.getText().substring(8).trim();
                if (StringUtils.equalsAny(mode, "true", "on")) {
                    silence.set(true);
                } else if (StringUtils.equalsAny(mode, "false", "off")) {
                    silence.set(false);
                }
            } else if (!silence.get()
                    || silentCommands.contains(command)
                    || message.getChat().isUserChat()
                    || message.getChatId() == -1001171141013L) {
                var handler = handlerMap.get(command);
                if (handler != null) {
                    handler.handle(update, bot);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private String getCommand(Update update) {
        var message = update.getMessage();
        if (message == null || !message.hasText()) {
            return null;
        }
        var command = firstNonNull(message.getText(), message.getCaption()).trim().split(" ")[0];
        var split = command.split("@");

        if (split.length == 1 || props.getIdentifier().equals(split[1])) {
            return split[0];
        } else {
            return null;
        }
    }

}
